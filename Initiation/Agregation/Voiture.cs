﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Initiation.Agregation
{
    // sealed : bloque l'héritage (une classe ne pourra pas hériter de Voiture)
    public sealed class Voiture
    {
        // Agégation forte : voiture ne peut exister sans moteur
        public Moteur Moteur { get; init; }

        public Voiture(Moteur moteur)
        {
            Moteur = moteur;
        }

        // Association : voiture utilise objet Parking de manière temporaire
        public void Garer(Parking parc)
        {
            parc.Garer();
        }
    }
}
