﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M3_Methodes
{
    internal class CSInter
    {
        static void Main2(string[] args)
        {
            int[] tab1 = { 10, 0, 20, 15 };
            int a1 = 10;
            int b1 = 20;

            // 1. Créez moi une méthode "Somme" qui retourne la somme de 2 entiers
            var somme = Somme(a1, b1);


            /* 2. Créez moi 2 méthodes "Afficher" (surcharge) :
             * a. Affiche un texte
             * b. Affiche les éléments de tab1
             */
            Afficher();
            Afficher(tab1);
            // 3. Créez moi une surcharge de la méthode "Somme" qui retourne la somme des éléments de tab1
            var sommme2 = Somme(tab1);
            // 4. Créez moi une méthode "Min" qui retourne l'élément le plus petit de tab1

            int min = Min(tab1);

            // 5. Créez moi une méthode "Moy" qui retourne la moyenne des éléments de tab1
            double moy = Moy(tab1);

            /* 6. Créez moi une méthode "Calculer" qui prend 1 tableau en entrée (tab1) et
             * qui calcule 2 valeurs : la somme et le minimum.
             * Ces valeurs devront être passées via des paramètres en sortie.
             */
            Calculer(tab1, out somme, out min);
        }   









        private static int Somme(int a, int b)
        {
            return a + b;
        }

        public static  void Afficher()
        {
            Console.WriteLine("Contenu");
        }
        public static void Afficher(int[] tab)
        {
            foreach(int a in tab)
            {
                Console.WriteLine(a);
            }
        }
        public static double Moy(int[] tab){
            return Somme(tab) / (double)tab.Length;
        }

        public static int Somme(int[] tab)
        {
            int somme = 0;

            foreach (int item in tab)
            {
                somme += item;
            }
            return somme;
        }


        public static int Min(int[] tab1)  // 4
        {
            //int min = tab1[0];

            //for (int i = 1; i < tab1.Length; i++)
            //{
            //    var element = tab1[i];

            //    if (element < min)
            //    {
            //         min = element;
            //    }
            //}

            int min = tab1[0];
            var i = 1;

            while (i < tab1.Length)
            {
                if(min > tab1[i++])
                {
                    min = tab1[i - 1];
                }
            }

            return min;
        }

        public static void Calculer(int[] tab1, out int somme, out int min )
        {
            somme = Somme(tab1);
            min = Min(tab1);
        }
    }
}















