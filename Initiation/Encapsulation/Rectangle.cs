﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Initiation.Encapsulation
{
    public class Rectangle
    {
        private long Longueur { get; set; }

        private long _largeur;
        private long Largeur
        {
            get
            {
                return _largeur;
            }
            set
            {
                if (value < 0) throw new ArgumentOutOfRangeException("La largeur ne peut être négative");
                else _largeur = value;
            }
        }

        public Rectangle(long longueur, long largeur)
        {
            Longueur = longueur;
            Largeur = largeur;
        }

        public Rectangle()
        {
            Longueur = 0;
            Largeur = 0;
        }

        public void Redim(long longueur, long largeur)
        {
            Longueur = longueur;
            Largeur = largeur;
        }

        public double Aire()
        {
            return Longueur * _largeur;
        }
    }
}
