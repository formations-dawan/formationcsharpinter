﻿namespace Initiation.Genericite.CasConcret
{
    public abstract class EntiteBase<T>
    {
        public T Id { get; set; }
    }
}