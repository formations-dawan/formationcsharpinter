﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Initiation.Genericite.CasConcret
{
    public interface IDao<TEntite, TCle> where TEntite : EntiteBase<TCle>
    {
        TEntite GetById(TCle id);
        int Insert(TEntite entite);
    }
}
