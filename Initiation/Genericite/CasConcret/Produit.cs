﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Initiation.Genericite.CasConcret
{
    public class Produit : EntiteBase<int>
    {
        public string Libelle { get; set; }

        public double Prix { get; set; }
    }
}
