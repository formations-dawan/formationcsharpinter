﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Initiation.Genericite
{
    public class Generic<TGenerique>
    {
        public TGenerique Donnees { get; set; }

        public Generic(TGenerique donnees)
        {
            Donnees = donnees;
        }

        public override string ToString()
        {
            return "Mon objet (data = " + Donnees + ")";
        }
    }
}
