﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Initiation.Heritage
{
    public class Loup : Animal
    {
        public Loup(string nom, int age) : base(nom, age)
        {
        }

        public override void Deplacer()
        {
            Console.WriteLine("Je cours");
        }

        public override void Espece()
        {
            base.Espece();
            Console.WriteLine("Je suis un loup.");
        }
    }
}
