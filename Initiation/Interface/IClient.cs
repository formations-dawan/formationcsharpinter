﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Initiation.Interface
{
    public interface IClient
    {
        void AjouterClient(Client c);

        void SupprimerClient(Client c);

        void MettreAJourClient(Client c);

        List<Client> RecupererTout();

        Client RecupererClient();
    }
}
