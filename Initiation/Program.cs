﻿using Initiation.Encapsulation;
using Initiation.Genericite;
using Initiation.Genericite.CasConcret;
using Initiation.Interface;
using Initiation.Polymorphisme;
using System;

namespace Initiation;

class Program
{
    public static void Main(string[] args)
    {
        #region Bases
        // See https://aka.ms/new-console-template for more information
        Console.WriteLine("Hello, World!");

        int[] ints = { 1, 2, 3 };
        int[] monTab = new int[5];
        monTab[0] = 50;
        monTab[1] = 15;

        Console.WriteLine(monTab[1]);

        monTab[2] = 13;
        monTab[3] = 1;
        monTab[4] = 3;

        Console.WriteLine(monTab.Length);

        // ++i
        //int i = 0;
        //i = i + 1;
        //Console.WriteLine(i);

        //// i++
        //i = 0;
        //Console.WriteLine(i);
        //i = i + 1;

        for(int i = 0; i < monTab.Length; ++i)
        {
            Console.WriteLine(monTab[i]);
        }

        foreach(var nombre in monTab)
        {
            Console.WriteLine(nombre);
        }

        for (int i = 0; i < monTab.Length; ++i)
        {
            var nombre = monTab[i];

            Console.WriteLine(nombre);
        }

        int[,] matrice = new int[2, 3];
        matrice[0, 0] = 10;
        matrice[0, 1] = 15;
        matrice[0, 2] = 1;
        matrice[1, 0] = 78;
        matrice[1, 1] = 145;
        matrice[1, 2] = 200;

        int[,] matrice2 = { { 10, 20, 30 }, { 18, 28, 38 } };

        for (int i = 0; i < matrice2.GetLength(0); ++i)
        {
            AfficherColonnes(matrice2, i);
        }

        foreach (var nombre in matrice2)
        {
            Console.WriteLine(nombre);
        }

        //Algo recherche élément
        string[] transports = { "Moto", "Voiture", "Camion", "Avion" };
        var trouve = false;
        Console.Write("Entrez le transport à rechercher :");
        var recherche = Console.ReadLine();

        for (int i = 0; i < transports.Length; ++i)
        {
            if (transports[i].Equals(recherche))
            {
                trouve = true;
                break;
            }
        }

        Console.WriteLine(trouve);

        //Méthodes
        Soustraction(5, 6);
        var res = Soustraction2(5, 6);

        Console.WriteLine(Somme(1, 2));

        var a1 = 10;
        var b1 = 13;

        Permutation(ref a1, ref b1);

        Console.WriteLine(a1);

        Console.ReadLine();

        double resMoy;

        var resMod = Calcul(12, 40, out double resSomme, out resMoy);

        Console.WriteLine(resSomme);

        var a2 = 56;
        var b2 = 78;
        var resMul = Multiplication(a2, b2);

        int age;
        var estMajeur = EstMajeur(out age);
        Console.WriteLine(estMajeur);
        Console.WriteLine(age);

        //Exceptions
        try
        {
            var resDiv = Division(null, 0);
        }
        catch (ArgumentOutOfRangeException ex)
        {
            Console.WriteLine(ex.Message);
        }
        catch (NullReferenceException)
        {
            // Pas besoin car déjà géré dans la méthode Division
        }
        catch (ArgumentException ex)
        {
            Console.WriteLine(ex.Message);
        }
        catch (DivideByZeroException)
        {
            Console.WriteLine("Calcul impossible : division par zéro");
        }
        catch (Exception ex)
        {
            Console.WriteLine("Calcul impossible" +  ex.Message);
        }
        #endregion

        #region POO
        //Encapsulation
        var rect = new Rectangle(56, 19);
        var aire = rect.Aire();
        rect.Redim(78, 5);
        aire = rect.Aire();

        //Interface
        IClient client = new DbClientService();
        client.AjouterClient(null);
        client.SupprimerClient(null);
        (client as DbClientService).GetNomClient(1);

        //Genericite
        var gen = new Generic<int>(2);
        gen.Donnees = 45;

        IDao<Produit, int> produitService = new ProduitDao();
        IDao<Commande, int> commandeService = new CommandeDao();

        produitService.GetById(1);
        commandeService.GetById(1);

        //Polymorphisme
        IPliable table = new Table();
        PolyTest.Acheter(table);
        #endregion
    }

    private static void AfficherColonnes(int[,] matrice2, int i)
    {
        for (int j = 0; j < matrice2.GetLength(1); ++j)
        {

            Console.WriteLine(matrice2[i, j]);
        }
    }

    public static void Soustraction(int a, int b)
    {
        Console.WriteLine(a - b);
    }

    public static int Soustraction2(int a, int b)
    {
        return a - b;
    }

    public static void AfficherTab(short[] tab)
    {
        foreach (var element in tab)
        {
            Console.WriteLine(element);
        }
    }

    public static int Somme(int a, int b, int c = 3)
    {
        return a + b + c;
    }

    public static void Permutation(ref int a, ref int b)
    {
        int c = a;
        a = b;
        b = c;
    }

    public static double Calcul(double a, double b, out double somme, out double moy)
    {
        somme = a + b;
        moy = (a + b) / 2;

        return a % b;
    }

    public static int Multiplication(in int a, in int b)
    {
        return a * b;
    }

    public static bool EstMajeur(out int age)
    {
        Console.Write("Entrez votre âge : ");
        var _age = Convert.ToInt32(Console.ReadLine());

        age = _age;

        if (_age >= 18)
            return true;

        return false;
    }

    public static double? Division(double? a, double? b)
    {
        if (a.HasValue)
            if (a > 5)
                throw new ArgumentOutOfRangeException("A est supérieur à 5");

        double? res = null;

        try
        {
            res = a.Value / b.Value;
        }
        catch (NullReferenceException)
        {
            // gestion exception null
        }
        finally
        {
            // Uniquement pour les ressources non managées qui doivent être nettoyées
        }

        return res;
    }
}

