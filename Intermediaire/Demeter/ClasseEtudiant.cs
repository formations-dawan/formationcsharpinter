﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intermediaire.Demeter
{
    internal class ClasseEtudiant
    {
        public IList<Etudiant> Etudiants { get; internal set; }

        public int CompterEtudiants()
        {
            return Etudiants.Count;
        }
    }
}
