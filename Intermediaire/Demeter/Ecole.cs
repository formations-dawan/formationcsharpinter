﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intermediaire.Demeter
{
    internal class Ecole
    {
        private List<Grade> _grades;

        //Cette méthode ne respecte pas la loi de Demeter
        /*public int CompterEtudiants()
        {
            int nombreEtu = 0;

            foreach (var grade in _grades)
            {
                foreach (var classe in grade.Classes)
                {
                    foreach (var etudiant in classe.Etudiants)
                    {
                        nombreEtu++;
                    }
                }
            }

            return nombreEtu;
        }*/

        public int CompterEtudiants()
        {
            int nombreEtu = 0;

            _grades.ForEach(g => nombreEtu = g.CompterEtudiants());

            return nombreEtu;
        }
    }
}
