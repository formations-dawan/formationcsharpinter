﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intermediaire.DesignPatterns.Adapter
{
    internal interface IJsonAdapter
    {
        string RecupererContactsJson(string cheminFichier);
    }
}
