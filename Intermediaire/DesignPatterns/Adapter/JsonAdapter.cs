﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Intermediaire.DesignPatterns.Adapter
{
    internal class JsonAdapter : IJsonAdapter
    {
        private IContactRepository xmlRepository;

        public JsonAdapter(IContactRepository xmlRepository)
        {
            this.xmlRepository = xmlRepository;
        }

        public string RecupererContactsJson(string cheminFichier)
        {
            string xml = xmlRepository.RecupererContactsXml(cheminFichier);

            //Transformation en JSON
            var contacts = xmlRepository.ConversionDepuisXml(xml);

            string resJson = null;
            DataContractJsonSerializer s = new DataContractJsonSerializer(contacts.GetType());
            using (var memStream = new MemoryStream())
            {
                s.WriteObject(memStream, contacts);
                resJson = Encoding.UTF8.GetString(memStream.ToArray());
            }

            return resJson;
        }
    }
}
