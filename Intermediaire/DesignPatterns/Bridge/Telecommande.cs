﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intermediaire.DesignPatterns.Bridge
{
    internal abstract class Telecommande
    {
        protected IAppareil appareil;

        public void DefinirAppareil(IAppareil appareil)
        {
            this.appareil = appareil;
        }

        public abstract void ChangerVolume(int v);

        public abstract void ChangerCanal(int c);

        public abstract void AllumerOuEteindre();
    }
}
