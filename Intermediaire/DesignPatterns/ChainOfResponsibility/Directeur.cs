﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intermediaire.DesignPatterns.ChainOfResponsibility
{
    internal class Directeur : MembreEquipe
    {
        public Directeur(string nom, MembreEquipe successeur) : base(nom, successeur)
        {
        }

        public override void HandlePlainte(Plainte requete)
        {
            Console.WriteLine("Traitement par le directeur");
            requete.EtatPlainte = Plainte.Etats.Ferme;
        }
    }
}
