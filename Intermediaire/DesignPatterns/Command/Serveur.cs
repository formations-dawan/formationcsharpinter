﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intermediaire.DesignPatterns.Command
{
    internal class Serveur //Invoker
    {
        public void PrendreCommande(ICommander commande)
        {
            commande.Executer();
        }
    }
}
