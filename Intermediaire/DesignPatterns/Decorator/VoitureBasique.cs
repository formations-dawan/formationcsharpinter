﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intermediaire.DesignPatterns.Decorator
{
    internal class VoitureBasique : IVoiture
    {
        public void Assembler()
        {
            Console.WriteLine("Voiture basique...");
        }
    }
}
