﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Intermediaire.DesignPatterns.Observer.Exemple1
{
    internal class Client : IObservateur
    {
        public int? Id { get; set; }

        public string Nom { get; set; }

        public string Email { get; set; }

        public Client(string nom, string email)
        {
            Nom = nom;
            Email = email;
        }

        public override bool Equals(object? obj)
        {
            return obj is Client client &&
                Id == client.Id;
        }

        public void MettreAJour(ISujet sujet)
        {
            Produit p = (Produit)sujet;

            var corpsEmail = "Bonjour,\n le prix du produit a été modifié : " + p.Prix;

            var m = new MailMessage("noreply@dawan.fr",
                Email,
                "Changement de prix du " + p.Description,
                corpsEmail);

            /*var client = new SmtpClient(Constantes.SERVEUR_SMTP);
            client.Credentials = CredentialCache.DefaultNetworkCredentials;
            client.Send(m);*/

            Console.WriteLine(m.Body);
        }
    }
}
