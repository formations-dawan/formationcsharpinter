﻿namespace Intermediaire.DesignPatterns.Observer.Exemple1
{
    internal interface IObservateur
    {
        void MettreAJour(ISujet sujet);
    }
}