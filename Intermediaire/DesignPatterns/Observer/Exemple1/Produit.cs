﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intermediaire.DesignPatterns.Observer.Exemple1
{
    internal class Produit : ISujet
    {
        public int? Id { get; set; }

        public string Description { get; set; }

        private double _prix;

        public double Prix
        {
            get { return _prix; }
            set { 
                _prix = value;
                Notifier(this);
            }
        }

        public List<IObservateur> Observateurs { get; set; }

        public Produit()
        {
            Observateurs = new List<IObservateur>();
        }

        public void Attacher(IObservateur observateur)
        {
            if(!Observateurs.Contains(observateur))
                Observateurs.Add(observateur);
        }

        public void Detacher(IObservateur observateur)
        {
            Observateurs.Remove(observateur);
        }

        public void Notifier(ISujet sujet)
        {
            foreach (var o in Observateurs)
                o.MettreAJour(sujet);
        }
    }
}
