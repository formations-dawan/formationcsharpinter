﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intermediaire.DesignPatterns.Observer.Exemple2
{
    internal class Contact : IObserver<ChangementPrixEvent>
    {
        public string Nom { get; set; }

        public void OnCompleted()
        {
            Debug.WriteLine("Terminé...");
        }

        public void OnError(Exception error)
        {
            Debug.WriteLine(error.Message);
        }

        public void OnNext(ChangementPrixEvent value)
        {
            Console.WriteLine(value.Date.ToString("dd/MM/yy") + " : " + value.MessageNotif);
        }
    }
}
