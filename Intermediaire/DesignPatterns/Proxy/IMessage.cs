﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intermediaire.DesignPatterns.Proxy
{
    internal interface IMessage
    {
        string RecupererContenu();
    }
}
