﻿// See https://aka.ms/new-console-template for more information


#region EntitesVsObjetsValeurs
using Intermediaire.DesignPatterns.Adapter;
using Intermediaire.DesignPatterns.ChainOfResponsibility;
using Intermediaire.DesignPatterns.Command;
using Intermediaire.DesignPatterns.Decorator;
using Intermediaire.DesignPatterns.Observer.Exemple1;
using Intermediaire.DesignPatterns.Observer.Exemple2;
using Intermediaire.DesignPatterns.Proxy;
using Intermediaire.EntitesVsObjetsValeurs;
using ContactObs = Intermediaire.DesignPatterns.Observer.Exemple2.Contact;


Entreprise e1 = new Entreprise(1, "Dawan", new Geolocation(3.3527, 48.8543));
var e2 = new Entreprise(2, "Jehann", new Geolocation(3.37, 48.8543));

int compParNom = e1.CompareTo(e2);
int compParLocalisation = e1.CompareToLocalisation(e2);

Console.WriteLine(compParNom + " | " + compParLocalisation);

List<Entreprise> entreprises = new List<Entreprise> { e1, e2 };
entreprises.Sort(); //tri par nom
entreprises.ForEach(e => Console.WriteLine(e.Nom));

Console.Read();
#endregion

#region "Design Patterns - Adapter"
IJsonAdapter adapter = new JsonAdapter(new ContactRepository());
//string json = adapter.RecupererContactsJson("contacts.xml");
//Console.WriteLine(json);
#endregion

#region "Design Patterns - Decorator"
IVoiture voiture = new VoitureBasique();

voiture = new SportDecorator(voiture);
voiture.Assembler();

voiture = new LuxeDecorator(voiture);
voiture.Assembler();

//autre solution
IVoiture voitureSportLuxe = new SportDecorator(new LuxeDecorator(new VoitureBasique()));
voitureSportLuxe.Assembler();
#endregion

#region "Design Patterns - Proxy"
IMessage message = new MessageProxy(new MessageUtilisateur("Bonjour"));
Console.WriteLine(message.RecupererContenu());
#endregion

#region "Design Patterns - Chain Of Responsibility"
MembreEquipe formateur = new Formateur("Jesus", new DirPeda("John Doe", new Directeur("Dupond", null)));

Console.WriteLine("--Plainte 1--");
formateur.HandlePlainte(new Plainte(123,
    Plainte.Types.Formateur,
    "AHHHHHHH!",
    Plainte.Etats.Ouvert));

Console.WriteLine("--Plainte 2--");
formateur.HandlePlainte(new Plainte(123,
    Plainte.Types.DirPeda,
    "AHHHHHHH!",
    Plainte.Etats.Ouvert));

Console.WriteLine("--Plainte 3--");
formateur.HandlePlainte(new Plainte(123,
    Plainte.Types.Directeur,
    "AHHHHHHH!",
    Plainte.Etats.Ouvert));
#endregion

#region "Design Patterns - Observer"
// Observateur 1
var produit = new Produit()
{
    Id = 1,
    Description = "RTX 4090",
    Prix = 1600
};

produit.Attacher(new Client("Jean", "jean-bonneau@aupoivre.fr") { Id = 1 });
produit.Attacher(new Client("Albert", "albert@camus.fr") { Id = 2 });

produit.Prix = 150; //ce changement de prix déclenche 2 notifications par email

// Observateur 2
var article = new Article()
{
    Description = "Chaise",
    Prix = 40
};

var disC1 = article.Subscribe(new ContactObs() { Nom = "John" });
var disC2 = article.Subscribe(new ContactObs() { Nom = "Jane" });

article.Prix = 30; //le changement déclenche les notifications

disC1.Dispose(); //C1 se désinscrit

article.Prix = 25;
#endregion

#region "Design Patterns - Command"
var chef = new ChefCuisinier();
var serveur = new Serveur();

ICommander commande = new CommandeDiner(chef, "Burger");
serveur.PrendreCommande(commande);
#endregion

Console.ReadLine();