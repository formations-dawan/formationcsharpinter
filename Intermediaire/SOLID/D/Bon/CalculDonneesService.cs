﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intermediaire.SOLID.D.Bon
{
    internal class CalculDonneesService
    {
        private readonly IUtilisateurRepository _repository;

        public CalculDonneesService(IUtilisateurRepository repository)
        {
            _repository = repository;
        }

        public object CalculPourUtilisateur(long id)
        {
            var u = _repository.FindById(id);
            //traitement
            return null; //retourner le résultat
        }
    }
}
