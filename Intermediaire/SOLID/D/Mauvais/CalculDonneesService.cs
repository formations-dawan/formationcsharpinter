﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intermediaire.SOLID.D.Mauvais
{
    internal class CalculDonneesService
    {
        //code sans injection de dépendance
        //la méthode dépend de l'objet UtilisateurRepository
        public object CalculPourUtilisateur(long id)
        {
            UtilisateurRepository utilisateurRepository = new UtilisateurRepository();
            var u = utilisateurRepository.FindById(id);
            //traitement
            return null; //retourner le résultat
        }
    }
}
