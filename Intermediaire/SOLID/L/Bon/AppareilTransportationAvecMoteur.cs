﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intermediaire.SOLID.L.Bon
{
    internal abstract class AppareilTransportationAvecMoteur : AppareilTransportation
    {
        public Moteur Moteur { get; set; }

        protected AppareilTransportationAvecMoteur(Moteur moteur)
        {
            Moteur = moteur;
        }

        public override void Bouger()
        {
            //Démarrer moteur
        }
    }
}
