﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intermediaire.SOLID.L.Mauvais
{
    internal abstract class AppareilTransportation
    {
        public string Nom { get; set; }

        public int Speed { get; set; }

        public Moteur Moteur { get; set; }

        public abstract void DemarrerMoteur();
    }
}
