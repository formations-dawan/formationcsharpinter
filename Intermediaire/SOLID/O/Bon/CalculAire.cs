﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intermediaire.SOLID.O.Bon
{
    internal class CalculAire
    {
        public double CalculerAire(IForme f)
        {
            return f.Aire();
        }
    }
}
