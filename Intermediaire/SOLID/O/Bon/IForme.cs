﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intermediaire.SOLID.O.Bon
{
    internal interface IForme
    {
        double Aire();
    }
}
