﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intermediaire.SOLID.O.Mauvais
{
    //Open-Closed
    //Ouvert pour l'extension
    //Fermé pour la modification (=> Non, car à chaque ajout d'une nouvelle forme,
    //on devra ajouter une nouvelle méthode pour le calcul de l'aire)
    internal class CalculAire
    {
        public double CalculAireRectangle(Rectangle r)
        {
            return r.Longueur * r.Largeur;
        }

        public double CalculAireCercle(Cercle c)
        {
            return Math.PI * Math.Pow(c.Rayon, 2);
        }
    }
}
