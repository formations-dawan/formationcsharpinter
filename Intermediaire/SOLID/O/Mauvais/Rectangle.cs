﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intermediaire.SOLID.O.Mauvais
{
    internal class Rectangle
    {
        public int Longueur { get; internal set; }
        public int Largeur { get; internal set; }
    }
}
