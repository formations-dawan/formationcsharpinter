﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intermediaire.TellDontAsk.Exemple1.Bon
{
    internal class CompteService : ICompteService
    {
        private readonly CompteRepository _compteRepository;

        public CompteService(CompteRepository compteRepository)
        {
            _compteRepository = compteRepository;
        }

        public void Retrait(int compteId, double montant)
        {
            var compte = _compteRepository.FindById(compteId);

            compte.Retrait(montant);

            _compteRepository.Save(compte);
        }
    }
}
