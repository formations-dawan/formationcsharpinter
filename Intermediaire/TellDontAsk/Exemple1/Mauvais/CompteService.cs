﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intermediaire.TellDontAsk.Exemple1.Mauvais
{
    internal class CompteService
    {
        private readonly CompteRepository _compteRepository;

        public CompteService(CompteRepository compteRepository)
        {
            _compteRepository = compteRepository;
        }

        public void Retrait(int compteId, double montant)
        {
            var compte = _compteRepository.FindById(compteId);

            if (compte.Solde < montant)
                throw new ArgumentException("Erreur : pas assez d'argent !");

            compte.Solde -= montant;

            _compteRepository.Save(compte);
        }
    }
}
